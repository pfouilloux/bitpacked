use extract::StructDetails;
use generate::macro_output;
use proc_macro::TokenStream;
use syn::parse_macro_input;

mod extract;
mod generate;

extern crate proc_macro;

#[proc_macro_attribute]
pub fn bitpacked(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let struct_details = parse_macro_input!(item as StructDetails);
    macro_output::generate(&struct_details).into()
}
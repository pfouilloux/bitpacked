#[macro_use]
mod errors;

mod bits_required;
mod field_details;
mod struct_details;
mod try_get_ident;
mod visibility;

pub use errors::{ExtractionFailure, Result};
pub use field_details::FieldDetails;
pub use struct_details::StructDetails;
pub use visibility::Visibility;

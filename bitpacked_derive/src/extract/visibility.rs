use quote::{format_ident, quote};
use syn::{
    parse_str,
    token::{Crate, In, Paren, Pub},
    Path, Type, VisCrate, VisPublic, VisRestricted,
};

#[derive(Debug, PartialEq)]
pub enum Visibility {
    Public,
    Inherited,
    Crate,
    PubCrate,
    PubSelf,
    PubSuper,
    PubModule(String),
}

impl From<syn::Visibility> for Visibility {
    fn from(vis: syn::Visibility) -> Self {
        match vis {
            syn::Visibility::Public(_) => Visibility::Public,
            syn::Visibility::Crate(_) => Visibility::Crate,
            syn::Visibility::Restricted(res) => res.into(),
            syn::Visibility::Inherited => Visibility::Inherited,
        }
    }
}

impl From<VisRestricted> for Visibility {
    fn from(vis: VisRestricted) -> Self {
        if let Some(_) = vis.in_token {
            let path = vis.path;
            return Visibility::PubModule(quote! { #path }.to_string().replace(" ", ""));
        } else if let Some(ident) = vis.path.get_ident() {
            if &format_ident!("self") == ident {
                return Visibility::PubSelf;
            } else if &format_ident!("crate") == ident {
                return Visibility::PubCrate;
            } else if &format_ident!("super") == ident {
                return Visibility::PubSuper;
            }
        }

        panic!("Invalid visibility!")
    }
}

impl Into<syn::Visibility> for Visibility {
    fn into(self) -> syn::Visibility {
        match self {
            Visibility::Public => syn::Visibility::Public(VisPublic {
                pub_token: Pub::default(),
            }),
            Visibility::Inherited => syn::Visibility::Inherited,
            Visibility::Crate => syn::Visibility::Crate(VisCrate {
                crate_token: Crate::default(),
            }),
            Visibility::PubCrate => {
                syn::Visibility::Restricted(vis_restricted(&format_ident!("crate").into()))
            }
            Visibility::PubSelf => {
                syn::Visibility::Restricted(vis_restricted(&format_ident!("self").into()))
            }
            Visibility::PubSuper => {
                syn::Visibility::Restricted(vis_restricted(&format_ident!("super").into()))
            }
            Visibility::PubModule(path) => syn::Visibility::Restricted(vis_restricted_module(path)),
        }
    }
}

fn vis_restricted(path: &Path) -> VisRestricted {
    VisRestricted {
        pub_token: Pub::default(),
        paren_token: Paren::default(),
        in_token: None,
        path: Box::new(path.clone().into()),
    }
}

fn vis_restricted_module(module: String) -> VisRestricted {
    if let Type::Path(type_path) = parse_str(module.as_str()).expect("String is not tokens") {
        VisRestricted {
            pub_token: Pub::default(),
            paren_token: Paren::default(),
            in_token: Some(In::default()),
            path: Box::new(type_path.path.into()),
        }
    } else {
        panic!("Failed to parse. String was not a TypePath")
    }
}

#[cfg(test)]
mod tests {
    use super::Visibility;
    use quote::format_ident;
    use syn::{
        parse_str,
        token::{Crate, In, Paren, Pub},
        Type, VisCrate, VisPublic, VisRestricted,
    };
    #[test]
    fn should_convert_syn_public_to_public_and_back() {
        let converted: Visibility = vis_public().into();
        assert_eq!(converted, Visibility::Public);

        let converted_back: syn::Visibility = Visibility::Public.into();
        assert_eq!(converted_back, vis_public());
    }

    #[test]
    fn should_convert_syn_crate_to_crate_and_back() {
        let converted: Visibility = vis_crate().into();
        assert_eq!(converted, Visibility::Crate);

        let converted_back: syn::Visibility = Visibility::Crate.into();
        assert_eq!(converted_back, vis_crate());
    }

    #[test]
    fn should_convert_syn_inherited_to_inherited_and_back() {
        let converted: Visibility = syn::Visibility::Inherited.into();
        assert_eq!(converted, Visibility::Inherited);

        let converted_back: syn::Visibility = Visibility::Inherited.into();
        assert_eq!(converted_back, syn::Visibility::Inherited);
    }

    #[test]
    fn should_convert_syn_pub_crate_to_pub_crate_and_back() {
        let converted: Visibility = vis_pub_crate().into();
        assert_eq!(converted, Visibility::PubCrate);

        let converted_back: syn::Visibility = Visibility::PubCrate.into();
        assert_eq!(converted_back, vis_pub_crate());
    }

    #[test]
    fn should_convert_syn_pub_self_to_pub_self_and_back() {
        let converted: Visibility = vis_pub_self().into();
        assert_eq!(converted, Visibility::PubSelf);

        let converted_back: syn::Visibility = Visibility::PubSelf.into();
        assert_eq!(converted_back, vis_pub_self());
    }

    #[test]
    fn should_convert_syn_pub_super_to_pub_super_and_back() {
        let converted: Visibility = vis_pub_super().into();
        assert_eq!(converted, Visibility::PubSuper);

        let converted_back: syn::Visibility = Visibility::PubSuper.into();
        assert_eq!(converted_back, vis_pub_super());
    }

    #[test]
    fn should_convert_syn_pub_module_to_pub_module_and_back() {
        let converted: Visibility = vis_pub_module("crate::extract").into();
        assert_eq!(
            converted,
            Visibility::PubModule("crate::extract".to_string())
        );

        let converted_back: syn::Visibility =
            Visibility::PubModule("crate::extract".to_string()).into();
        assert_eq!(converted_back, vis_pub_module("crate::extract"));
    }

    fn vis_public() -> syn::Visibility {
        syn::Visibility::Public(VisPublic {
            pub_token: Pub::default(),
        })
    }

    fn vis_crate() -> syn::Visibility {
        syn::Visibility::Crate(VisCrate {
            crate_token: Crate::default(),
        })
    }

    fn vis_pub_crate() -> syn::Visibility {
        syn::Visibility::Restricted(VisRestricted {
            pub_token: Pub::default(),
            paren_token: Paren::default(),
            in_token: None,
            path: Box::new(format_ident!("crate").into()),
        })
    }

    fn vis_pub_self() -> syn::Visibility {
        syn::Visibility::Restricted(VisRestricted {
            pub_token: Pub::default(),
            paren_token: Paren::default(),
            in_token: None,
            path: Box::new(format_ident!("self").into()),
        })
    }

    fn vis_pub_super() -> syn::Visibility {
        syn::Visibility::Restricted(VisRestricted {
            pub_token: Pub::default(),
            paren_token: Paren::default(),
            in_token: None,
            path: Box::new(format_ident!("super").into()),
        })
    }

    fn vis_pub_module(path_str: &str) -> syn::Visibility {
        if let Type::Path(type_path) = parse_str(path_str).expect("String is not tokens") {
            syn::Visibility::Restricted(VisRestricted {
                pub_token: Pub::default(),
                paren_token: Paren::default(),
                in_token: Some(In::default()),
                path: Box::new(type_path.path.clone()),
            })
        } else {
            panic!("Failed to parse. String was not a TypePath")
        }
    }
}

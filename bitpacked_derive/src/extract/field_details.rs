use super::{
    bits_required::BitsRequiredSource, try_get_ident::TryGetIdent, ExtractionFailure, Result,
};
use proc_macro2::TokenStream;
use quote::quote;
use std::convert::TryFrom;
use syn::Field;

pub struct FieldDetails {
    pub ident: String,
    pub ty: TokenStream,
    pub mem: usize,
}

impl TryFrom<&Field> for FieldDetails {
    type Error = ExtractionFailure;

    fn try_from(value: &Field) -> Result<Self> {
        let ty = &value.ty;

        Ok(FieldDetails {
            ident: value.try_get_ident().map(|ok| ok.to_string())?,
            ty: quote! { #ty },
            mem: value.extract_bits_required()?,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::FieldDetails;
    use quote::quote;
    use std::convert::{TryFrom, TryInto};
    use std::fmt::Debug;
    use syn::{parse2, FieldsNamed, FieldsUnnamed};

    #[test]
    fn should_convert_field_into_field_details() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = 8] a: u8 }}.into())
            .expect("Should have parsed")
            .named[0];

        let details: FieldDetails = field.try_into().expect("Should have succeeded");

        assert_eq!(
            details,
            FieldDetails {
                ident: "a".to_string(),
                ty: quote! { u8 },
                mem: 8
            }
        )
    }

    #[test]
    fn should_fail_to_convert_unnamed_field() {
        let field = &parse2::<FieldsUnnamed>(quote! {(u8)}.into())
            .expect("Should have parsed")
            .unnamed[0];

        let error = FieldDetails::try_from(field).expect_err("Should have failed");

        assert_eq!(error, invalid_field!("Unnamed fields are not supported"))
    }

    #[test]
    fn should_fail_to_convert_field_that_does_not_have_bits_attribute() {
        let field = &parse2::<FieldsNamed>(quote! {{ a: u8 }}.into())
            .expect("Should have parsed")
            .named[0];

        let error = FieldDetails::try_from(field).expect_err("Should have failed");

        assert_eq!(error, missing_attribute!("#[bits = ..]", "a"))
    }

    impl Debug for FieldDetails {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(
                f,
                "FieldDetails {{ ident: {}, ty: {}, mem: {} }}",
                self.ident, self.ty, self.mem
            )
        }
    }

    impl PartialEq for FieldDetails {
        fn eq(&self, other: &FieldDetails) -> bool {
            self.ident == other.ident
                && self.ty.to_string() == other.ty.to_string()
                && self.mem == other.mem
        }
    }
}

use std::convert::TryFrom;

use super::{ExtractionFailure, FieldDetails};
use proc_macro2::TokenStream;
use quote::quote;
use syn::{
    braced,
    parse::{Parse, ParseStream},
    Field, Ident, Result, Token, Visibility,
};

pub struct StructDetails {
    pub visibility: TokenStream,
    pub ident: TokenStream,
    pub fields: Vec<FieldDetails>,
}

impl Parse for StructDetails {
    fn parse(input: ParseStream) -> Result<Self> {
        let visibility = input.parse::<Visibility>()?;
        input.parse::<Token![struct]>()?;
        let ident = input.parse::<Ident>()?;

        Ok(StructDetails {
            visibility: quote!(#visibility),
            ident: quote!(#ident),
            fields: extract_field_details(input)?,
        })
    }
}

fn extract_field_details(input: ParseStream) -> Result<Vec<FieldDetails>> {
    let content;
    braced!(content in input);
    let fields = content.parse_terminated::<Field, Token![,]>(Field::parse_named)?;

    let mut details = Vec::<FieldDetails>::new();
    let mut errors = Vec::<ExtractionFailure>::new();
    for field in fields {
        match FieldDetails::try_from(&field) {
            Ok(field_details) => details.push(field_details),
            Err(failure) => errors.push(failure),
        }
    }

    if errors.len() > 0 {
        Err(input.error(format!(
            "{}",
            aggregate_extraction_failures!("Failed to parse one or more fields", errors)
        )))
    } else {
        Ok(details)
    }
}

#[cfg(test)]
mod tests {
    use super::StructDetails;
    use crate::extract::FieldDetails;
    use quote::quote;
    use std::fmt::Debug;
    use syn::parse2;

    #[test]
    fn should_extract_struct_details() {
        let input = quote! { pub(crate) struct Hello { #[bits = 8] a: u8 }};

        let details = parse2::<StructDetails>(input).expect("Should have succeeded");

        assert_eq!(
            details,
            StructDetails {
                visibility: quote! { pub(crate) },
                ident: quote! { Hello },
                fields: vec![FieldDetails {
                    ident: "a".to_string(),
                    ty: quote! { u8 },
                    mem: 8
                }],
            }
        )
    }

    #[test]
    fn should_fail_if_missing_visibility() {
        let input = quote! { Things struct BadStuff };

        let error = parse2::<StructDetails>(input).expect_err("Should have errored");

        assert_eq!(
            error.to_compile_error().to_string(),
            quote! { compile_error!{"expected `struct`"} }.to_string()
        )
    }

    #[test]
    fn should_fail_if_missing_ident() {
        let input = quote! { struct {} };

        let error = parse2::<StructDetails>(input).expect_err("Should have errored");

        assert_eq!(
            error.to_compile_error().to_string(),
            quote! { compile_error!{"expected identifier"} }.to_string()
        )
    }

    #[test]
    fn should_fail_if_parentesis_struct() {
        let input = quote! { struct Bad(u8) };

        let error = parse2::<StructDetails>(input).expect_err("Should have errored");

        assert_eq!(
            error.to_compile_error().to_string(),
            quote! { compile_error!{"expected curly braces"} }.to_string()
        )
    }

    #[test]
    fn should_fail_if_empty_struct() {
        let input = quote! { struct Bad };

        let error = parse2::<StructDetails>(input).expect_err("Should have errored");

        assert_eq!(
            error.to_compile_error().to_string(),
            quote! { compile_error!{"unexpected end of input, expected curly braces"} }.to_string()
        )
    }

    #[test]
    fn should_fail_if_given_enum() {
        let input = quote! { enum Bad { Thing } };

        let error = parse2::<StructDetails>(input).expect_err("Should have errored");

        assert_eq!(
            error.to_compile_error().to_string(),
            quote! { compile_error!{"expected `struct`"} }.to_string()
        )
    }

    #[test]
    fn should_fail_if_given_union() {
        let input = quote! { union Bad {} };

        let error = parse2::<StructDetails>(input).expect_err("Should have errored");

        assert_eq!(
            error.to_compile_error().to_string(),
            quote! { compile_error!{"expected `struct`"} }.to_string()
        )
    }

    impl Debug for StructDetails {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(
                f,
                "StructDetails {{ visibility: {}, ident: {}, fields: {:?} }}",
                self.visibility, self.ident, self.fields
            )
        }
    }

    impl PartialEq for StructDetails {
        fn eq(&self, other: &StructDetails) -> bool {
            self.visibility.to_string() == other.visibility.to_string()
                && self.ident.to_string() == other.ident.to_string()
                && self.fields == other.fields
        }
    }
}

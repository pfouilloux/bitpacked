use super::{try_get_ident::TryGetIdent, Result};
use quote::{format_ident, quote};
use std::str;
use syn::{Attribute, Field, Lit, LitInt};
use syn::{Meta, MetaNameValue};

pub trait BitsRequiredSource {
    fn extract_bits_required(&self) -> Result<usize>;
}

impl BitsRequiredSource for Field {
    fn extract_bits_required(&self) -> Result<usize> {
        let attr = find_bits_attribute(self)?;
        let name_value = parse_bits_attribute(&attr, &self.try_get_ident()?.to_string())?;
        extract_bits_required(&name_value, &self.try_get_ident()?.to_string())
    }
}

fn find_bits_attribute(field: &Field) -> Result<Attribute> {
    let bit_attributes = field
        .attrs
        .iter()
        .filter(|it| it.path.is_ident(&format_ident!("{}", BITS)))
        .map(|it| it.clone())
        .collect::<Vec<_>>();

    match bit_attributes.len() {
        1 => Ok(bit_attributes[0].clone()),
        0 => Err(missing_attribute!(
            format!("#[{} = ..]", BITS),
            field.try_get_ident()?
        )),
        _ => Err(duplicate_attribute!(
            format!("#[{} = ..]", BITS),
            field.try_get_ident()?
        )),
    }
}

fn parse_bits_attribute(attr: &Attribute, field_ident: &String) -> Result<MetaNameValue> {
    let meta = attr
        .parse_meta()
        .map_err(|e| invalid_attribute!(quote! { #attr }.to_string(), field_ident, "{:?}", e))?;

    match meta {
        Meta::List(_) => Err(invalid_attribute!(
            format_attribute(attr),
            field_ident,
            "please use name-value syntax to declare the required number of bits. ex: #[bits = 4]"
        )),
        Meta::Path(_) => Err(invalid_attribute!(
            format_attribute(attr),
            field_ident,
            "please provide the required number of bits. ex: #[bits = 4]"
        )),
        Meta::NameValue(name_value) => Ok(name_value),
    }
}

fn extract_bits_required(name_value: &MetaNameValue, field_ident: &String) -> Result<usize> {
    match &name_value.lit {
        Lit::Str(it) => format_invalid_literal_error(format!("\"{}\"", it.value()), field_ident),
        Lit::ByteStr(it) => format_invalid_literal_error(
            str::from_utf8(&it.value())
                .map_or(format!("{:?}", &it.value()), |ok| format!("b\"{}\"", ok)),
            field_ident,
        ),
        Lit::Byte(it) => format_invalid_literal_error(
            str::from_utf8(&[it.value()])
                .map_or(format!("{:?}", &it.value()), |ok| format!("b\'{}\'", ok)),
            field_ident,
        ),
        Lit::Char(it) => format_invalid_literal_error(format!("'{}'", it.value()), field_ident),
        Lit::Int(it) => parse_bits_required(it, field_ident),
        Lit::Float(it) => format_invalid_literal_error(it.base10_digits().to_string(), field_ident),
        Lit::Bool(it) => format_invalid_literal_error(format!("{}", it.value), field_ident),
        Lit::Verbatim(it) => format_invalid_literal_error(it.to_string(), field_ident),
    }
}

fn parse_bits_required(int: &LitInt, field_ident: &String) -> Result<usize> {
    int.base10_parse::<usize>()
        .map_err(|e| {
            invalid_attribute!(
                format!("#[bits = {}]", int.base10_digits()),
                field_ident,
                "{:?} - please provide a non-zero positive integer literal ex: #[bits = 4]",
                e
            )
        })
        .and_then(|v| {
            if v == 0 {
                Err(invalid_attribute!(
                    format!("#[bits = {}]", v),
                    field_ident,
                    "please provide a non-zero positive integer literal ex: #[bits = 4]"
                ))
            } else {
                Ok(v)
            }
        })
}

fn format_attribute(attr: &Attribute) -> String {
    let token_stream = quote! { #attr };
    token_stream.to_string().replace(" ", "")
}

fn format_invalid_literal_error(val: String, field_ident: &String) -> Result<usize> {
    Err(invalid_attribute!(
        format!("#[{} = {}]", BITS, val),
        field_ident,
        "please provide a non-zero positive integer literal ex: #[bits = 4]"
    ))
}

const BITS: &str = "bits";

#[cfg(test)]
mod tests {
    use super::{extract_bits_required, BitsRequiredSource};
    use proc_macro2::Literal;
    use quote::{format_ident, quote};
    use syn::{parse2, token::Eq, FieldsNamed, Lit, MetaNameValue};

    #[test]
    fn should_extract_bits_required() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = 7] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let bits_required = field
            .extract_bits_required()
            .expect("Should have succeeded");

        assert_eq!(bits_required, 7)
    }

    #[test]
    fn should_fail_when_bits_attribute_is_declared_more_than_once() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = 7] #[bits = 8] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(error, duplicate_attribute!("#[bits = ..]", "x"))
    }

    #[test]
    fn should_fail_when_bits_attribute_is_not_declared() {
        let field = &parse2::<FieldsNamed>(quote! {{ x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(error, missing_attribute!("#[bits = ..]", "x"))
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_path_syntax() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits]",
                "x",
                "please provide the required number of bits. ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_list_syntax() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits(12)] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits(12)]",
                "x",
                "please use name-value syntax to declare the required number of bits. ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_a_string() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = "hello"] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = \"hello\"]",
                "x",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_a_byte_string() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = b"hello"] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = b\"hello\"]",
                "x",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_a_char() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = 'h'] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = 'h']",
                "x",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_a_float() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = 0.1234] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = 0.1234]",
                "x",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_a_byte() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = b't'] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = b't']",
                "x",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_zero() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = 0] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = 0]",
                "x",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_a_negative_integer() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = -42] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = -42]",
                "x",
                "Error(\"invalid digit found in string\") - please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_a_bool() {
        let field = &parse2::<FieldsNamed>(quote! {{ #[bits = true] x: u8 }})
            .expect("Should have parsed")
            .named[0];

        let error = field
            .extract_bits_required()
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = true]",
                "x",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }

    #[test]
    fn should_fail_when_bits_attribute_declared_with_something_syn_could_not_parse() {
        let meta_name_value = MetaNameValue {
            path: format_ident!("bits").into(),
            eq_token: Eq::default(),
            lit: Lit::Verbatim(Literal::string("Something bad")),
        };

        let error = extract_bits_required(&meta_name_value, &"test_field".to_string())
            .expect_err("Should have failed");

        assert_eq!(
            error,
            invalid_attribute!(
                "#[bits = \"Something bad\"]",
                "test_field",
                "please provide a non-zero positive integer literal ex: #[bits = 4]"
            )
        )
    }
}

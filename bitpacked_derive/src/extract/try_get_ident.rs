use super::Result;
use syn::{Field, Ident};

pub trait TryGetIdent {
    fn try_get_ident(&self) -> Result<&Ident>;
}

impl TryGetIdent for Field {
    fn try_get_ident(&self) -> Result<&Ident> {
        self.ident
            .as_ref()
            .ok_or_else(|| invalid_field!("Unnamed fields are not supported"))
    }
}

#[cfg(test)]
mod tests {
    use super::TryGetIdent;
    use quote::{format_ident, quote};
    use syn::{parse2, FieldsNamed, FieldsUnnamed};

    #[test]
    fn should_get_ident_named_field() {
        let field = &parse2::<FieldsNamed>(quote! {{ x: u8 }}.into())
            .expect("Should have parsed")
            .named[0];

        let ident = field.try_get_ident().expect("Should have succeeded");

        assert_eq!(ident, &format_ident!("x"))
    }

    #[test]
    fn should_fail_to_get_ident_from_unnamed_field() {
        let field = &parse2::<FieldsUnnamed>(quote! {(u8)})
            .expect("Should have parsed token stream")
            .unnamed[0];

        let error = field.try_get_ident().expect_err("Should have failed");

        assert_eq!(error, invalid_field!("Unnamed fields are not supported"))
    }
}

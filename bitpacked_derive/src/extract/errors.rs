use std::fmt::Display;
use thiserror::*;

pub type Result<T> = std::result::Result<T, ExtractionFailure>;

#[derive(Error, Debug, Clone, PartialEq)]
pub enum ExtractionFailure {
    #[error("Invalid field: {reason}")]
    InvalidField { reason: String },
    #[error("Missing attribute {attr} on field {field}")]
    MissingAttribute { attr: String, field: String },
    #[error("Attribute {attr} on field {field} does not support multiple declarations")]
    DuplicateAttribute { attr: String, field: String },
    #[error("Invalid attribute {attr} on field {field}: {reason}")]
    InvalidAttribute {
        attr: String,
        field: String,
        reason: String,
    },
    #[error("{msg}:\n{}", errors)]
    AggregateError { msg: String, errors: Errors },
}

#[derive(Debug, Clone, PartialEq)]
pub struct Errors {
    errors: Vec<ExtractionFailure>,
}

impl From<Vec<ExtractionFailure>> for Errors {
    fn from(errors: Vec<ExtractionFailure>) -> Self {
        Errors { errors }
    }
}

impl Into<Vec<ExtractionFailure>> for Errors {
    fn into(self) -> Vec<ExtractionFailure> {
        self.errors
    }
}

impl Display for Errors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for error in self.errors.iter() {
            writeln!(f, "\t--{}", error)?;
        }

        Ok(())
    }
}

macro_rules! invalid_field {
    ($($arg:tt)+) => {{
        let reason = std::fmt::format(std::format_args!($($arg)*));
        $crate::extract::errors::ExtractionFailure::InvalidField { reason }
    }}
}

macro_rules! missing_attribute {
    ($attr:expr, $field:expr) => {{
        $crate::extract::errors::ExtractionFailure::MissingAttribute {
            attr: $attr.to_string(),
            field: $field.to_string(),
        }
    }};
}

macro_rules! duplicate_attribute {
    ($attr:expr, $field:expr) => {{
        $crate::extract::errors::ExtractionFailure::DuplicateAttribute {
            attr: $attr.to_string(),
            field: $field.to_string(),
        }
    }};
}

macro_rules! invalid_attribute {
    ($attr:expr, $field:expr, $($arg:tt)+) => {
        $crate::extract::errors::ExtractionFailure::InvalidAttribute {
            attr: $attr.to_string(),
            field: $field.to_string(),
            reason: std::fmt::format(std::format_args!($($arg)*)),
        }
    };
}

macro_rules! aggregate_extraction_failures {
    ($msg:expr, [$($error:expr),+]) => {{
        let mut errors = Vec::new();
        $(errors.push($error);)+

        aggregate_extraction_failures!($msg, errors)
    }};
    ($msg:expr, $errors:expr) => {{
        $crate::extract::errors::ExtractionFailure::AggregateError {
            msg: $msg.to_string(),
            errors: $errors.into(),
        }
    }};
}

#[cfg(test)]
mod tests {
    use super::ExtractionFailure;

    #[test]
    fn should_create_invalid_fields_error_with_reason() {
        let error = invalid_field!("test {}", 123);

        assert_eq!(
            error,
            ExtractionFailure::InvalidField {
                reason: "test 123".to_string()
            }
        );
        assert_eq!(
            format!("{}", error),
            "Invalid field: test 123".to_string()
        );
    }

    #[test]
    fn should_create_missing_attribute_error_with_attribute_and_field_name() {
        let error = missing_attribute!("test_attr", "some_field");
        assert_eq!(
            error,
            ExtractionFailure::MissingAttribute {
                attr: "test_attr".to_string(),
                field: "some_field".to_string()
            }
        );
        assert_eq!(
            format!("{}", error),
            "Missing attribute test_attr on field some_field".to_string()
        );
    }

    #[test]
    fn should_create_duplicate_attribute_error_with_attribute_and_field_name() {
        let error = duplicate_attribute!("test_attr", "some_field");
        assert_eq!(
            error,
            ExtractionFailure::DuplicateAttribute {
                attr: "test_attr".to_string(),
                field: "some_field".to_string()
            }
        );
        assert_eq!(
            format!("{}", error),
            "Attribute test_attr on field some_field does not support multiple declarations"
                .to_string()
        );
    }

    #[test]
    fn should_create_invalid_attribute_error_with_attribute_name_field_name_and_reason() {
        let error = invalid_attribute!("test_attr", "test_field", "boom!");

        assert_eq!(
            error,
            ExtractionFailure::InvalidAttribute {
                attr: "test_attr".to_string(),
                field: "test_field".to_string(),
                reason: "boom!".to_string()
            }
        );
        assert_eq!(
            format!("{}", error),
            "Invalid attribute test_attr on field test_field: boom!".to_string()
        );
    }

    #[test]
    fn should_create_aggregate_error_with_message_from_vector() {
        let error = aggregate_extraction_failures!(
            "Bad stuff happened",
            vec![invalid_field!("test {}", 123)]
        );

        assert_eq!(
            error,
            ExtractionFailure::AggregateError {
                msg: "Bad stuff happened".to_string(),
                errors: vec![invalid_field!("test {}", 123)].into()
            }
        );
        assert_eq!(
            format!("{}", error),
            "Bad stuff happened:\n\t--Invalid field: test 123\n".to_string()
        );
    }

    #[test]
    fn should_create_aggregate_error_with_message_from_errors() {
        let error = aggregate_extraction_failures!(
            "Bad stuff happened",
            [
                invalid_field!("test {}", 123),
                invalid_field!("test2 {}", 1234)
            ]
        );

        assert_eq!(
            error,
            ExtractionFailure::AggregateError {
                msg: "Bad stuff happened".to_string(),
                errors: vec![
                    invalid_field!("test {}", 123),
                    invalid_field!("test2 {}", 1234)
                ]
                .into()
            }
        );
        assert_eq!(
            format!("{}", error),
            "Bad stuff happened:\n\t--Invalid field: test 123\n\t--Invalid field: test2 1234\n"
                .to_string()
        );
    }
}

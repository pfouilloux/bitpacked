use super::{bitpacked_trait_impl, bits_required, struct_declaration, struct_implementation};
use crate::StructDetails;
use proc_macro2::TokenStream;
use quote::quote;

pub fn generate(input: &StructDetails) -> TokenStream {
    let mem_req = bits_required::calculate(&input.fields);
    let struct_decl = struct_declaration::generate(&input.visibility, &input.ident, &mem_req);
    let bitpacked_impl = bitpacked_trait_impl::generate(&input.ident, &mem_req);
    let struct_impl =
        struct_implementation::generate(&input.visibility, &input.ident, &input.fields);

    quote! {
        #struct_decl
        #bitpacked_impl
        #struct_impl
    }
}

#[cfg(test)]
mod tests {
    use crate::extract::{FieldDetails, StructDetails};
    use pretty_assertions::assert_eq;
    use quote::quote;

    #[test]
    fn should_derive_implementation() {
        let struct_details = StructDetails {
            visibility: quote! { pub },
            ident: quote! { Hello },
            fields: vec![FieldDetails {
                ident: "x".to_string(),
                ty: quote! { u8 },
                mem: 5,
            }],
        };

        let output = super::generate(&struct_details);

        assert_eq! {
            output.to_string(),
            quote!{
                #[derive(Debug, PartialEq)]
                pub struct Hello {
                    bits: bitpacked::bitvec::BitArr!(for 5usize, in bitpacked::bitvec::Lsb0, u8),
                }

                impl bitpacked::BitPacked for Hello {
                    const CAPACITY: usize = 5usize;

                    fn new() -> Self {
                        use bitpacked::bitvec::BitStore;
                        Hello {
                            bits: bitpacked::bitvec::bitarr![bitpacked::bitvec::Lsb0, u8; 0; Hello::CAPACITY]
                        }
                    }

                    fn mut_bit_slice(
                        &mut self,
                        range: std::ops::Range<usize>,
                    ) -> &mut bitpacked::bitvec::BitSlice<bitpacked::bitvec::Lsb0, u8> {
                        &mut self.bits[range]
                    }

                    fn bit_slice(
                        &mut self,
                        range: std::ops::Range<usize>,
                    ) -> &bitpacked::bitvec::BitSlice<bitpacked::bitvec::Lsb0, u8> {
                        &self.bits[range]
                    }
                }

                impl Hello {
                    pub fn new() -> Self {
                        bitpacked::BitPacked::new()
                    }

                    pub fn get_x(&self) -> u8 {
                        use bitpacked::bitvec::BitField;

                        self.bits[0usize..5usize].load_be()
                    }

                    pub fn set_x(&mut self, val: u8) -> Result<(), bitpacked::BitpackingFailure> {
                        use bitpacked::{bitvec::BitField, BitPacked};

                        bitpacked::internal::validate_fits_in_bitstore(&val.to_be_bytes(), Self::CAPACITY)?;

                        self.bits[0usize..5usize].store_be(val);
                        Ok(())
                    }
                }
            }.to_string()
        }
    }
}

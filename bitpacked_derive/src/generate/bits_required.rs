use crate::extract::FieldDetails;

pub fn calculate(fields: &Vec<FieldDetails>) -> usize {
    fields.iter().map(|field| field.mem).sum()
}

#[cfg(test)]
mod tests {
    use crate::extract::FieldDetails;
    use quote::quote;

    #[test]
    fn should_calculate_bits_required() {
        let fields = vec![
            FieldDetails {
                ident: "x".to_string(),
                ty: quote! { u8 },
                mem: 5,
            },
            FieldDetails {
                ident: "y".to_string(),
                ty: quote! { u8 },
                mem: 2,
            },
        ];
        assert_eq!(super::calculate(&fields), 7);
    }
}

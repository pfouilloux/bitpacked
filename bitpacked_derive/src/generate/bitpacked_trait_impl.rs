use proc_macro2::TokenStream;
use quote::quote;

pub fn generate(ident: &TokenStream, bits_size: &usize) -> TokenStream {
    quote! {
        impl bitpacked::BitPacked for #ident {
            const CAPACITY: usize = #bits_size;

            fn new() -> Self {
                use bitpacked::bitvec::BitStore;
                #ident {
                    bits: bitpacked::bitvec::bitarr![bitpacked::bitvec::Lsb0, u8; 0; #ident::CAPACITY]
                }
            }

            fn mut_bit_slice(
                &mut self,
                range: std::ops::Range<usize>,
            ) -> &mut bitpacked::bitvec::BitSlice<bitpacked::bitvec::Lsb0, u8> {
                &mut self.bits[range]
            }

            fn bit_slice(
                &mut self,
                range: std::ops::Range<usize>,
            ) -> &bitpacked::bitvec::BitSlice<bitpacked::bitvec::Lsb0, u8> {
                &self.bits[range]
            }
        }
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;
    use quote::quote;

    #[test]
    fn should_derive_implementation() {
        let output = super::generate(&quote!(Hello), &5);

        assert_eq! {
            output.to_string(),
            quote!{
                impl bitpacked::BitPacked for Hello {
                    const CAPACITY: usize = 5usize;

                    fn new() -> Self {
                        use bitpacked::bitvec::BitStore;
                        Hello {
                            bits: bitpacked::bitvec::bitarr![bitpacked::bitvec::Lsb0, u8; 0; Hello::CAPACITY]
                        }
                    }

                    fn mut_bit_slice(
                        &mut self,
                        range: std::ops::Range<usize>,
                    ) -> &mut bitpacked::bitvec::BitSlice<bitpacked::bitvec::Lsb0, u8> {
                        &mut self.bits[range]
                    }

                    fn bit_slice(
                        &mut self,
                        range: std::ops::Range<usize>,
                    ) -> &bitpacked::bitvec::BitSlice<bitpacked::bitvec::Lsb0, u8> {
                        &self.bits[range]
                    }
                }
            }.to_string()
        }
    }
}

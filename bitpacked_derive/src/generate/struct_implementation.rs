use super::accessors;
use crate::extract::FieldDetails;
use proc_macro2::TokenStream;
use quote::quote;

pub fn generate(
    vis: &TokenStream,
    struct_ident: &TokenStream,
    fields: &Vec<FieldDetails>,
) -> TokenStream {
    let accessors = accessors::generate(vis, fields);

    quote! {
        impl #struct_ident {
            #vis fn new() -> Self {
                bitpacked::BitPacked::new()
            }

            #(#accessors)*
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::extract::FieldDetails;
    use quote::quote;

    #[test]
    fn should_generate_struct_implementation() {
        let struct_impl = super::generate(
            &quote! {pub},
            &quote! {Hello},
            &vec![FieldDetails {
                ident: "x".to_string(),
                ty: quote! { u8 },
                mem: 5,
            }],
        );

        assert_eq!(
            struct_impl.to_string(),
            quote! {
                impl Hello {
                    pub fn new() -> Self {
                        bitpacked::BitPacked::new()
                    }

                    pub fn get_x(&self) -> u8 {
                        use bitpacked::bitvec::BitField;

                        self.bits[0usize..5usize].load_be()
                    }

                    pub fn set_x(&mut self, val: u8) -> Result<(), bitpacked::BitpackingFailure> {
                        use bitpacked::{bitvec::BitField, BitPacked};

                        bitpacked::internal::validate_fits_in_bitstore(&val.to_be_bytes(), Self::CAPACITY)?;

                        self.bits[0usize..5usize].store_be(val);
                        Ok(())
                    }
                }
            }.to_string()
        )
    }
}

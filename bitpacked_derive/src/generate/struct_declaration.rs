use proc_macro2::TokenStream;
use quote::quote;

pub fn generate(vis: &TokenStream, ident: &TokenStream, bits_size: &usize) -> TokenStream {
    quote! {
        #[derive(Debug, PartialEq)]
        #vis struct #ident {
            bits: bitpacked::bitvec::BitArr!(for #bits_size, in bitpacked::bitvec::Lsb0, u8),
        }
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;
    use quote::quote;

    #[test]
    fn should_derive_implementation() {
        let output = super::generate(&quote! { pub }, &quote! { Hello }, &5);

        assert_eq! {
            output.to_string(),
            quote!{
                #[derive(Debug, PartialEq)]
                pub struct Hello {
                    bits: bitpacked::bitvec::BitArr!(for 5usize, in bitpacked::bitvec::Lsb0, u8),
                }
            }.to_string()
        }
    }
}

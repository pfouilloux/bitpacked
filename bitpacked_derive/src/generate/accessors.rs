use crate::extract::FieldDetails;
use proc_macro2::{Ident, TokenStream};
use quote::{format_ident, quote};

pub fn generate(vis: &TokenStream, fields: &Vec<FieldDetails>) -> Vec<TokenStream> {
    let mut accessors = Vec::<TokenStream>::new();
    let mut cur_bits = 0usize;
    for field in fields {
        let accessor = generate_accessors(cur_bits, field, vis);
        accessors.push(accessor);
        cur_bits += field.mem;
    }

    accessors
}

fn generate_accessors(start_idx: usize, field: &FieldDetails, vis: &TokenStream) -> TokenStream {
    let getter = generate_getter(
        start_idx,
        start_idx + field.mem,
        vis,
        format_ident!("get_{}", &field.ident),
        &field.ty,
    );

    let setter = generate_setter(
        start_idx,
        start_idx + field.mem,
        vis,
        format_ident!("set_{}", &field.ident),
        &field.ty,
    );

    quote! {
        #getter
        #setter
    }
}

fn generate_getter(
    start_idx: usize,
    end_idx: usize,
    vis: &TokenStream,
    ident: Ident,
    typ: &TokenStream,
) -> TokenStream {
    quote! {
        #vis fn #ident(&self) -> #typ {
            use bitpacked::bitvec::BitField;

            self.bits[#start_idx..#end_idx].load_be()
        }
    }
}

fn generate_setter(
    start_idx: usize,
    end_idx: usize,
    vis: &TokenStream,
    ident: Ident,
    typ: &TokenStream,
) -> TokenStream {
    quote! {
        #vis fn #ident(&mut self, val: #typ) -> Result<(), bitpacked::BitpackingFailure> {
            use bitpacked::{bitvec::BitField, BitPacked};

            bitpacked::internal::validate_fits_in_bitstore(&val.to_be_bytes(), Self::CAPACITY)?;

            self.bits[#start_idx..#end_idx].store_be(val);
            Ok(())
        }
    }
}

#[cfg(test)]
mod test {
    use crate::extract::FieldDetails;
    use pretty_assertions::assert_eq;
    use quote::quote;

    #[test]
    fn should_generate_accessors() {
        let fields = vec![
            FieldDetails {
                ident: "x".to_string(),
                ty: quote! { u8 },
                mem: 5,
            },
            FieldDetails {
                ident: "y".to_string(),
                ty: quote! { u8 },
                mem: 2,
            },
        ];

        let output = super::generate(&quote! { pub }, &fields);

        assert_eq! {
            quote!{ #(#output)* }.to_string(),
            quote!{
                pub fn get_x(&self) -> u8 {
                    use bitpacked::bitvec::BitField;

                    self.bits[0usize..5usize].load_be()
                }

                pub fn set_x(&mut self, val: u8) -> Result<(), bitpacked::BitpackingFailure> {
                    use bitpacked::{bitvec::BitField, BitPacked};

                    bitpacked::internal::validate_fits_in_bitstore(&val.to_be_bytes(), Self::CAPACITY)?;

                    self.bits[0usize..5usize].store_be(val);
                    Ok(())
                }

                pub fn get_y(&self) -> u8 {
                    use bitpacked::bitvec::BitField;

                    self.bits[5usize..7usize].load_be()
                }

                pub fn set_y(&mut self, val: u8) -> Result<(), bitpacked::BitpackingFailure> {
                    use bitpacked::{bitvec::BitField, BitPacked};

                    bitpacked::internal::validate_fits_in_bitstore(&val.to_be_bytes(), Self::CAPACITY)?;

                    self.bits[5usize..7usize].store_be(val);
                    Ok(())
                }
            }.to_string()
        }
    }
}

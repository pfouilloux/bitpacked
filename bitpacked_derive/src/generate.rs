pub mod macro_output;

mod accessors;
mod bitpacked_trait_impl;
mod bits_required;
mod struct_declaration;
mod struct_implementation;

use super::errors::Result;
use bitvec::{order::Lsb0, slice::BitSlice};
use bytes::Bytes;
use std::fmt::Debug;
use std::ops::Range;

pub trait BitPacked: Debug + PartialEq + Sized {
    const CAPACITY: usize;

    fn new() -> Self;
    fn mut_bit_slice(&mut self, range: Range<usize>) -> &mut BitSlice<Lsb0, u8>;
    fn bit_slice(&mut self, range: Range<usize>) -> &BitSlice<Lsb0, u8>;
}

pub trait ToBytes {
    fn to_bytes(&mut self) -> Bytes;
}

pub trait FromBytes: Sized {
    fn from_bytes(bytes: &Bytes) -> Result<Self>;
}

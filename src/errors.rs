use thiserror::*;

pub type Result<T> = std::result::Result<T, BitpackingFailure>;

#[derive(Error, Debug, PartialEq)]
pub enum BitpackingFailure {
    #[error("{length} bits exceed maximum capacity of {capacity} bits")]
    BitsExceedCapacity { length: usize, capacity: usize },
}

#[cfg(test)]
mod tests {
    use super::BitpackingFailure;

    #[test]
    fn should_create_bits_exceed_capacity_error_with_length_of_input_and_capacity() {
        assert_eq!(
            format!(
                "{}",
                BitpackingFailure::BitsExceedCapacity {
                    length: 8,
                    capacity: 3
                }
            ),
            "8 bits exceed maximum capacity of 3 bits".to_string()
        );
    }
}

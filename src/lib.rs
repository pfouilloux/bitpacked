#[macro_use]
#[allow(unused_imports)]
extern crate bitpacker_derive;

pub mod internal;

mod conversion;
mod errors;
mod traits;

pub use errors::BitpackingFailure;
pub use traits::*;

// Reexport our macros
pub use bitpacker_derive::*;

// Reexport the bitvec crate
pub mod bitvec {
    pub use bitvec::prelude::*;
}

// Reexport the bytes crate
pub mod bytes {
    pub use bytes::*;
}

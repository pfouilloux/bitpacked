use crate::{errors::Result, BitpackingFailure};

pub fn validate_fits_in_bitstore(bytes: &[u8], capacity: usize) -> Result<()> {
    let length = calculate_bytes_size_in_bits(bytes);
    if length <= capacity {
        Ok(())
    } else {
        Err(BitpackingFailure::BitsExceedCapacity { length, capacity })
    }
}

fn calculate_bytes_size_in_bits(bytes: &[u8]) -> usize {
    (bytes.len() * 8) - bytes.last().map_or(0, |it| it.leading_zeros()) as usize
}

#[cfg(test)]
mod tests {
    use super::validate_fits_in_bitstore;
    use crate::BitpackingFailure;

    #[test]
    fn should_succeed_if_bits_are_within_capacity() {
        let result = validate_fits_in_bitstore(&[0b0000_1111u8], 8);
        assert!(result.is_ok());
    }

    #[test]
    fn should_fail_if_bits_are_over_capacity() {
        let error = validate_fits_in_bitstore(&[0b1111u8], 2).expect_err("Should have failed");

        assert_eq!(
            error,
            BitpackingFailure::BitsExceedCapacity {
                length: 4,
                capacity: 2,
            }
        )
    }
}

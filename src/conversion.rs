use super::internal;
use crate::{bitvec::BitField, bytes::Bytes, errors::Result, BitPacked, FromBytes, ToBytes};
use std::cmp::min;
use std::ops::Range;

impl<T: BitPacked> ToBytes for T {
    fn to_bytes(&mut self) -> Bytes {
        Bytes::copy_from_slice(self.bit_slice(0..Self::CAPACITY).as_raw_slice())
    }
}

impl<T: BitPacked> FromBytes for T {
    fn from_bytes(bytes: &Bytes) -> Result<Self> {
        internal::validate_fits_in_bitstore(&bytes, Self::CAPACITY)?;

        let mut converted = Self::new();
        iter_over_bytes(Self::CAPACITY, bytes.len(), |idx, bit_range| {
            converted.mut_bit_slice(bit_range).store_be(bytes[idx]);
        });

        Ok(converted)
    }
}

fn iter_over_bits<T: FnMut(usize, Range<usize>) -> ()>(capacity: usize, mut op: T) {
    let byte_capacity = capacity / 8;
    for idx in 0..byte_capacity {
        op(idx, (idx * 8)..((idx + 1) * 8));
    }

    let remainder = capacity % 8;
    if remainder > 0 {
        op(
            byte_capacity,
            byte_capacity * 8..byte_capacity * 8 + remainder,
        );
    }
}

fn iter_over_bytes<T: FnMut(usize, Range<usize>) -> ()>(bit_capacity: usize, bytes: usize, op: T) {
    let capacity = min(bit_capacity, bytes * 8);
    iter_over_bits(capacity, op);
}

#[cfg(test)]
mod tests {
    use super::{BitPacked, FromBytes, ToBytes};
    use crate::bitvec::BitSlice;
    use crate::{
        bitvec::{bitarr, BitArr, BitField, BitStore, Lsb0},
        bytes::Bytes,
    };
    use std::ops::Range;

    #[test]
    fn should_convert_to_bytes() {
        let bytes = TestBitPacked::copy_from_byte(0b10101).to_bytes();
        assert_eq!(bytes, Bytes::copy_from_slice(&[0b10101]));
    }

    #[test]
    fn should_convert_from_bytes() {
        let bitpacked = TestBitPacked::from_bytes(&Bytes::copy_from_slice(&[0b10101]))
            .expect("Should have succeeded");
        assert_eq!(bitpacked, TestBitPacked::copy_from_byte(0b10101));
    }

    #[test]
    fn should_convert_from_empty_bytes() {
        let bitpacked = TestBitPacked::from_bytes(&Bytes::new()).expect("Should have succeeded");
        assert_eq!(bitpacked, TestBitPacked::new())
    }

    #[derive(Debug, PartialEq)]
    struct TestBitPacked {
        bits: BitArr!(for 5usize, in Lsb0, u8),
    }

    impl TestBitPacked {
        fn copy_from_byte(x: u8) -> Self {
            let mut new = TestBitPacked::new();

            new.bits[0usize..5usize].store_be(x);

            new
        }
    }

    impl BitPacked for TestBitPacked {
        const CAPACITY: usize = 5;
        fn new() -> Self {
            TestBitPacked {
                bits: bitarr![Lsb0, u8; 0; TestBitPacked::CAPACITY],
            }
        }

        fn mut_bit_slice(&mut self, range: Range<usize>) -> &mut BitSlice<Lsb0, u8> {
            &mut self.bits[range]
        }

        fn bit_slice(&mut self, range: Range<usize>) -> &BitSlice<Lsb0, u8> {
            &self.bits[range]
        }
    }
}

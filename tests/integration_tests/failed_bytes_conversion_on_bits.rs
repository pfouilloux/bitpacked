use bitpacked::{bitpacked, BitpackingFailure, FromBytes};
use bytes::Bytes;

#[bitpacked]
pub struct Data {
    #[bits = 11]
    a: u8,
}

fn main() {
    let error = Data::from_bytes(&Bytes::copy_from_slice(&[0b1111_1111, 0b1111_1111]))
        .expect_err("Should have failed");

    assert_eq!(
        error,
        BitpackingFailure::BitsExceedCapacity {
            length: 16,
            capacity: 11
        }
    );
}

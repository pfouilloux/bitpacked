use bitpacked::{bitpacked, FromBytes, ToBytes};
use bytes::Bytes;

#[bitpacked]
pub struct Data {
    #[bits = 5]
    a: u8,
}

fn main() {
    let mut data = Data::new();

    data.set_a(0b10101)
        .expect("Setting 'a' should have succeeded");
    assert_eq!(data.to_bytes(), Bytes::copy_from_slice(&[0b10101]));

    let from_bytes =
        Data::from_bytes(&Bytes::copy_from_slice(&[0b10101])).expect("Should have succeeded");
    assert_eq!(from_bytes, data);
}

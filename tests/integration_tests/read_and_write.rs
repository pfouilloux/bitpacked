use bitpacked::bitpacked;

#[bitpacked]
pub struct Data {
    #[bits = 4]
    a: u8,
}

fn main() {
    let mut data = Data::new();

    data.set_a(0b1010).expect("Should have succeeded");
    assert_eq!(data.get_a(), 0b1010);
}

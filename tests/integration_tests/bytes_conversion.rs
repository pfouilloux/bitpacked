use bitpacked::{bitpacked, FromBytes, ToBytes};
use bytes::Bytes;

#[bitpacked]
pub struct Data {
    #[bits = 4]
    a: u8,
    #[bits = 1]
    b: u8,
    #[bits = 9]
    c: u16,
    #[bits = 18]
    d: u32,
}

fn main() {
    let mut data = Data::new();

    data.set_a(0b1010)
        .expect("Setting 'a' should have succeeded");
    data.set_b(0b1).expect("Setting 'b' should have succeeded");
    data.set_c(0b1010_1100_1)
        .expect("Setting 'c' should have succeeded");
    data.set_d(0b1010_1100_0011_0101_10)
        .expect("Setting 'd' should have succeeded");

    assert_eq!(
        data.to_bytes(),
        Bytes::copy_from_slice(&[0b1011_1010, 0b10011001, 0b10110000, 0b11010110])
    );

    let from_bytes = Data::from_bytes(&Bytes::copy_from_slice(&[
        0b1011_1010,
        0b10011001,
        0b10110000,
        0b11010110,
    ]))
    .expect("Should have succeeded");
    assert_eq!(from_bytes, data);
}

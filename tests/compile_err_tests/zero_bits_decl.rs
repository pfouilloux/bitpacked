use bitpacked::bitpacked;

#[bitpacked]
pub struct Data {
    #[bits = 0]
    a: u8,
}

fn main() {}

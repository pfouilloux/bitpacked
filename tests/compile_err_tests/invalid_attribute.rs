use bitpacked::bitpacked;

#[bitpacked]
pub struct Data {
    #[banana]
    a: u8,
}

fn main() {}

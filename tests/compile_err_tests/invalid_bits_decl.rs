use bitpacked::bitpacked;

#[bitpacked]
pub struct Data {
    #[bits]
    a: u8,
}

fn main() {}

#[test]
fn test_suite() {
    let t = trybuild::TestCases::new();
    t.pass("tests/integration_tests/*");
    t.compile_fail("tests/compile_err_tests/*.rs");
}

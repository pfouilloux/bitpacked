# securefmt
#### [![Build Status]][actions] [![Latest Version]][crates.io] [![Docs]][docs.rs]
[Build Status]: https://gitlab.com/pfouilloux/bitpacked/badges/master/pipeline.svg
[actions]: https://gitlab.com/pfouilloux/bitpacked/-/pipelines
[Latest Version]: https://img.shields.io/crates/v/bitpacked.svg
[crates.io]: https://crates.io/crates/bitpacked
[Docs]: https://docs.rs/bitpacked/badge.svg
[docs.rs]: https://docs.rs/bitpacked

Packs bits
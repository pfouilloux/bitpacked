ROOT_DIR:=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

.PHONY: clean
clean:
	cargo clean

.PHONY: build
build:
	cargo build --color always --verbose

.PHONY: test
test:
	cargo test --color always --all --all-features --verbose

.PHONY: docs
docs:
	cargo doc --all-features --verbose